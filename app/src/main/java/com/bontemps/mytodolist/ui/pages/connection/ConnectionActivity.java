package com.bontemps.mytodolist.ui.pages.connection;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bontemps.mytodolist.R;

public class ConnectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
    }
}