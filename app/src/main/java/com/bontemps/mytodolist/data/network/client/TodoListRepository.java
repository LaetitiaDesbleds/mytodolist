package com.bontemps.mytodolist.data.network.client;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bontemps.mytodolist.data.network.model.CreateUser;
import com.bontemps.mytodolist.data.network.model.CreateUserResponse;
import com.bontemps.mytodolist.data.network.model.LoginUser;
import com.bontemps.mytodolist.data.network.model.LoginUserResponse;
import com.bontemps.mytodolist.data.network.model.PostTask;
import com.bontemps.mytodolist.data.network.model.PostTaskResponse;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

public class TodoListRepository {
    private static final String BASE_URL = "http://gcoding.fr:8080/";

    private TodoListService todoListService;
    private MutableLiveData<CreateUserResponse> createUserResponseLiveData;
    private MutableLiveData<LoginUserResponse> loginUserResponseLiveData;
    private MutableLiveData<PostTaskResponse> postTaskResponseLiveData;

    public TodoListRepository() {
        createUserResponseLiveData = new MutableLiveData<>();
        loginUserResponseLiveData = new MutableLiveData<>();
        postTaskResponseLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        todoListService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TodoListService.class);

    }

    public void createUser(String email, String password) {
        CreateUser createUser = new CreateUser(email, password);
        todoListService.createUser(createUser)
                .enqueue(new Callback<CreateUserResponse>() {
                    @Override
                    public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                        if (response.body() != null) {
                            createUserResponseLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                        createUserResponseLiveData.postValue(null);
                    }
                });
    }

    public void loginUser(String email, String password) {
        LoginUser loginUser = new LoginUser(email, password);
        todoListService.loginUser(loginUser)
                .enqueue(new Callback<LoginUserResponse>() {
                    @Override
                    public void onResponse(Call<LoginUserResponse> call, Response<LoginUserResponse> response) {
                        if (response.body() != null) {
                            loginUserResponseLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginUserResponse> call, Throwable t) {
                        loginUserResponseLiveData.postValue(null);
                    }
                });
    }

    public void postTask(String token, String content) {
        PostTask postTask = new PostTask(content);
        todoListService.postTask("Bearer " + token, postTask)
                .enqueue(new Callback<PostTaskResponse>() {
                    @Override
                    public void onResponse(Call<PostTaskResponse> call, Response<PostTaskResponse> response) {
                        if (response.body() != null) {
                            postTaskResponseLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<PostTaskResponse> call, Throwable t) {
                        postTaskResponseLiveData.postValue(null);
                    }
                });
    }

    public LiveData<CreateUserResponse> getCreateUserResponseLiveData() {
        return createUserResponseLiveData;
    }

    public LiveData<LoginUserResponse> getLoginUserResponseLiveData() {
        return loginUserResponseLiveData;
    }

    public LiveData<PostTaskResponse> getPostTaskResponseLiveData() {
        return postTaskResponseLiveData;
    }
}
