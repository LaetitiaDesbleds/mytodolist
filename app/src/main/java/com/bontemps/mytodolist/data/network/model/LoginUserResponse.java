package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class LoginUserResponse {
    @SerializedName("access_token")
    private String token;

    public String getToken() {
        return token;
    }
}
