package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class LoginUser {
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    public LoginUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
