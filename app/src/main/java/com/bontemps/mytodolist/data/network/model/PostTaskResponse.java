package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class PostTaskResponse {
    @SerializedName("id")
    private final int id;
    @SerializedName("content")
    private final String content;
    @SerializedName("done")
    private final Boolean done;

    public PostTaskResponse(int id, String content, Boolean done) {
        this.id = id;
        this.content = content;
        this.done = done;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public Boolean getDone() {
        return done;
    }
}
