package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class CreateUserResponse {
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
