package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class CreateUser {
    @SerializedName("email")
    private final String email;
    @SerializedName("password")
    private final String password;

    public CreateUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}