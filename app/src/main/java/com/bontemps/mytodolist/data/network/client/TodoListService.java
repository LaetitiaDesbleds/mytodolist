package com.bontemps.mytodolist.data.network.client;

import com.bontemps.mytodolist.data.network.model.CreateUser;
import com.bontemps.mytodolist.data.network.model.CreateUserResponse;
import com.bontemps.mytodolist.data.network.model.LoginUser;
import com.bontemps.mytodolist.data.network.model.LoginUserResponse;
import com.bontemps.mytodolist.data.network.model.PostTask;
import com.bontemps.mytodolist.data.network.model.PostTaskResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TodoListService {
    @POST("user")
    Call<CreateUserResponse> createUser(@Body CreateUser createUser);
    @POST("user/login")
    Call<LoginUserResponse> loginUser(@Body LoginUser loginUser);
    @POST("task")
    Call<PostTaskResponse> postTask(@Header("Authorization") String token, @Body PostTask postTask);
}
