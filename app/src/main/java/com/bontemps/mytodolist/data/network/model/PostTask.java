package com.bontemps.mytodolist.data.network.model;

import com.google.gson.annotations.SerializedName;

public class PostTask {
    @SerializedName("content")
    private final String content;

    public PostTask(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
